/*
 * File:   main.c
 * Author: eduardo kamada
 *
 * Created on 28 de Mar�o de 2017, 23:53
 * 
 * Conhecimentos pr�vios necess�rios: 
 * Setar configura��o de pinos no pic
 * Interrup��es no pic: interrup��o de timer, de comunica��o e externa de pino
 * Uso da biblioteca PLIB(facilita para setar configura��es
 * No��o de fun��o em C
 * L�gica de programa��o
 * Leitura de datasheet do pic18f2550
 */
    
#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <plib/usart.h>
#include <plib/pwm.h>
#include <plib/timers.h>
#include <string.h>
#include <pic18f2550.h>
#include "config.h"
#include <stdbool.h>

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.


//defines
#define _XTAL_FREQ 20000000 //frequ�ncia do cristal
#define BAUDRATE 57600
#define BRG_VAL 21
#define TIMER0_START_REG 0x3C8C//40500(5ms) - 9E34 // 15500(10ms) - 3C8C
#define ESTOURO 0XFFF2

//fun��es
void interrupt Controle();
void InitializeUSART(void);
void definir_PINOS();
void configTimer0();
void configTimer1();
void configTimer2();
void configTimer3();
void Definir_PWM(void);
void Interpreta_RX();
void Controle_Motores(int L1, int L2);
void A_pwm(int duty1, int duty2);
void setar_PWM();

//var�aveis globais
int Speed_M1 = 0;
int Speed_M2 = 0;
int A_M1 = 0;
int A_M2 = 0;
int Rotacao_M1 = 0;
int Rotacao_M2 = 0;
int cntChar = 0;
int Flag_Read = 0;
int Size_RX = 0; 
int Flag_RX = 0;
char RXMessage[30];

//variaveis para encoder 
unsigned int contador_M1 = 0x00, contador_M2 = 0x00;
int borda_M1 = 0, borda_M2 = 0;
int reduction_M1 = 50, reduction_M2 = 100;
unsigned int p1_M1 = 0;
unsigned int p1_M2 = 0; 
unsigned int p2_M1 = 0;
unsigned int p2_M2 = 0;
int flag_controle = 0;
int Estouro_T1 = 0, Estouro_T2 = 0;
float ciclo_M1 = 0, ciclo_M2 = 0;
float freq_M1 = 0, freq_M2 = 0;
float RPM_M1 = 0, RPM_M2 = 0;
unsigned char buffer_M1[30], buffer_M2[30];
//Configura��es de pinos

void interrupt Controle()
{
    if(INTCONbits.TMR0IF)                                   //Se houver estouro no timer0
    {
        INTCONbits.TMR0IF = 0;                              //zera o timer
        flag_controle = 1;                                  //ativa o flag do looping de controle
        WriteTimer0(TIMER0_START_REG);                      //seta o timer para 10 ms
    }
    else{                                                   //se nao estourar o timer 
        if(INTCONbits.INT0IF){                              //Quando houver interrup��o externa no pino RB0
            INTCONbits.INT0IF = 0;                          //limpar a interrup��o
            borda_M1++;                                     //contador para borda de subida do motor 1
            if(borda_M1 == 1){                              //Primeira borda de subida
                WriteTimer1(0x0000);                        //Zera o timer
                p1_M1 = ReadTimer1();                       //seta a refer�ncia para calculo de periodo
            }
            if(borda_M1 == 2){                              //Segunda borda de subida
                if(Estouro_T1 == 1){                        //Se o TIMER1 estourar
                    p2_M1 = ESTOURO + ReadTimer1();         //Somar o estouro 
                    WriteTimer1(0x0000);
                    borda_M1 = 0;                           //Resetar contagem de borda do motor 1
                    contador_M1 = p2_M2 - p1_M1;            //C�lculo de per�odo
                }else{                                      //Caso n�o haja estouro do TIMER1
                    p2_M1 = ReadTimer1();                   //Leitura da borda 2 vale o TIMER1
                    WriteTimer1(0x0000);                    //zerar TIMER1
                    borda_M1 = 0;                           //zerar contagem da borda 1
                    contador_M1 = p2_M1 - p1_M1;            //Per�odo do sinal do motor 1
                }
                Estouro_T1 = 0;                             //Contador de estouro
            }
        }
        if(INTCON3bits.INT1F){                              //Interrup��o externa no pino RB1
            INTCON3bits.INT1F = 0;                          //Reseta interrup��o
            borda_M2++;                                     //Contador para borda de subida o motor 2
            if(borda_M2 == 1){                              //Primeira borda de subida
                WriteTimer3(0x0000);
                p1_M2 = ReadTimer3();                       //Refer�ncia para o motor 2
            }
            if(borda_M2 == 2){                              //Segunda borda de subida
                if(Estouro_T2 == 1){                        //Se o TIMER3 estourar
                    p2_M2 = ESTOURO + ReadTimer3();         //Somar o estouro 
                    WriteTimer3(0x0000);
                    borda_M2 = 0;                           //Zera contagem da borda do motor 2
                    contador_M2 = p2_M2 - p1_M2;            //Periodo do motor 2
                }else{                                      //Caso n�o haja estouro do TIMER3
                    p2_M2 = ReadTimer3();                   //Vale a leitura do TIMER3
                    WriteTimer3(0x0000);
                    borda_M2 = 0;                           //Resetar contagem de bordas
                    contador_M2 = p2_M2 - p1_M2;            //Periodo do motor 2
                }
                Estouro_T2 = 0;                             //Resetar flag de estouro
            }
        }
        if(PIR1bits.TMR1IF){                                //Se houver estouro do TIMER1
            PIR1bits.TMR1IF = 0;
            //Estouro_T1++;
            WriteTimer1(0x0000);
        }
        if(PIR2bits.TMR3IF){                                //Se houver estouro do TIMER3
            PIR2bits.TMR3IF = 0;
            //Estouro_T2++;
            WriteTimer3(0x0000);
        }
        if(PIR1bits.RCIF){                                  //Se houver recep��o de dados
            PIR1bits.RCIF = 0;                              //Reseta a recep��o
            if((cntChar == 0) && (ReadUSART() == '$')){     //Caso o char enviado comece por $
                Flag_Read = 1;                              //Ativar flag de leitura
        }
        if(Flag_Read == 1){
            RXMessage[cntChar] = ReadUSART();               //Ler caracter enviado
            if(RXMessage[cntChar] == '#'){                  //Caso o caracter seja "#"
                Flag_Read = 0;                              //Reseta o flag de leitura
                RXMessage[cntChar + 1] = '\0';
                Size_RX = cntChar - 1;
                cntChar = 0;
                Flag_RX = 1;                                //Ativa o flag de recep��o
            }
            else{                                           //Caso o char n�o for "#"
                cntChar++;                                  //somar no cntchar
            }
        }
    }
    }
}
void Definir_PINOS(void){                                   //Defini��o de pinos e configura��es
    //Definir terminais dos pino RA
    TRISAbits.RA0 = 0; // led para verificar RX
    TRISAbits.RA2 = 0; //Digital
    TRISAbits.RA3 = 0; //Digital
    TRISAbits.RA4 = 0; //Digital
    TRISAbits.RA5 = 0; //Digital
    //Definir terminais dos pinos RC
    TRISCbits.RC2 = 0; //Digital
    TRISCbits.RC6 = 0; //Digital - PINO TX(Transmissor)
    TRISCbits.RC7 = 1; //Digital - PINO RX(Receptor)
    //Definir terminais dos pinos RB
    TRISBbits.RB0 = 1; //Digital
    TRISBbits.RB1 = 1;
    TRISBbits.RB3 = 0;
    TRISBbits.RB4 = 0;
    //Seta toda as portas como digitais.
    ADCON1bits.PCFG0 = 1; //Digital
    ADCON1bits.PCFG1 = 1; //Digital
    ADCON1bits.PCFG2 = 1; //Digital
    ADCON1bits.PCFG3 = 1; //Digital
    RCONbits.IPEN = 1;
    INTCONbits.PEIE = 0;
    INTCONbits.GIE = 1;      
    INTCON2bits.INTEDG0 = 1;
    INTCON2bits.INTEDG1 = 1;
}
void configTimer0(){                                        //Fun��o para configurar o timer 0
    OpenTimer0(TIMER_INT_ON & T0_16BIT & T0_EDGE_RISE & T0_PS_1_1);   
    INTCONbits.INT0E = 1;
    T0CONbits.T0CS = 0;
    WriteTimer0(TIMER0_START_REG);
}
void configTimer1(){                                        //Fun��o para configurar o timer 1
    OpenTimer1(TIMER_INT_ON & T0_16BIT & T0_SOURCE_INT & T0_PS_1_1);
    INTCON3bits.INT1E = 1;
    T1CONbits.TMR1CS = 0;
    WriteTimer1(0x0000);
}
void configTimer2(){                                        //Fun��o para configurar o timer 2
    OpenTimer2(TIMER_INT_OFF & T0_PS_1_16); //Setar prescale  
    WriteTimer2(0x0000);
}
void configTimer3(){                                        //Fun��o para configurar o timer 3
    OpenTimer3(TIMER_INT_ON & T0_16BIT & T0_SOURCE_INT & T0_PS_1_1);
    INTCON3bits.INT1E = 1;
    T3CONbits.TMR3CS = 0;
    WriteTimer3(0x0000);
}
void Definir_PWM(){                                        //Define o per�odo do pwm
    OpenPWM1(249); //setar per�odo do pwm1
    OpenPWM2(249); //setar per�odo do pwm2   
    //--Colocar f�rmula aqui--
}
void InitializeUSART(void){                                //Ativa a comunica��o USART
    OpenUSART(USART_TX_INT_OFF &
             USART_RX_INT_ON &
             USART_ASYNCH_MODE &
             USART_EIGHT_BIT &
             USART_CONT_RX &
             USART_BRGH_HIGH,BRG_VAL );  
}
void Interpreta_RX(){                                       //Fun��o para transformar a mensagem CHAR em INT
        if (Size_RX == 13){                                 //recep��o de mensagem de tamanho 13 caracteres
            Rotacao_M1 = RXMessage[1] - 48;
            Rotacao_M2 = RXMessage[8] - 48;
            Speed_M1 = (RXMessage[3] - 48)*1000 + (RXMessage[4] - 48)*100 + (RXMessage[5] - 48)*10 + (RXMessage[6] - 48);
            Speed_M2 = (RXMessage[10] - 48)*1000 + (RXMessage[11] - 48)*100 + (RXMessage[12] - 48)*10 + (RXMessage[13] - 48);
            PORTAbits.RA0 = !PORTAbits.RA0;
        }       
        /*para essa convers�o, foi utilizada a tabela ASCII no qual 0 em decimal vale 48 char*/
}
void Controle_Motores(int L1, INT L2){
    /*O controle de motor funciona com os bits 0 e 1, sendo possivel setar para frente e tras
     nos dois motores do rob� atrav�s das condi��es abaixo*/
    if(L1==1){              //Se L1 == 1, Motor_2 gira para esquerda
        PORTAbits.RA2 = 1;      //Sinal logico alto na porta D1
        PORTAbits.RA3 = 0;      //Sinal logico baixo na porta D2
    }else if(L1==0){        //Se L1 ==0, Motor_2 gira para direita
        PORTAbits.RA2 = 0;  //Sinal logico baixo na porta D1
        PORTAbits.RA3 = 1;  //Sinal logico alto na porta D2
    }
    if(L2==1){              //Se L2 == 1, Motor_1 gira para esquerda
        PORTAbits.RA4 = 1;      //Sinal logico alto na porta D3
        PORTAbits.RA5 = 0;      //Sinal logico baixo na porta D4
    }else if(L2==0){        //Se L2==0, Motor_1 gira para direita 
        PORTAbits.RA4 = 0;  //Sinal logico baixo na porta D3
        PORTAbits.RA5 = 1;  //Sinal logico alto na porta D4
    }
}
void setar_PWM(){
    /*Essa fun��o seta o duty cycle do pwm, por�m, como h� a biblioteca do 
     PLIB, somente � necessaria a fun��o SetDCPWMx que facilita setar a rota��o do motor*/
    SetDCPWM1(Speed_M1);       //Comando para setar o "tempo" de sinal l�gico alto do PWM1
    SetDCPWM2(Speed_M2);       //Comando para setar o "tempo" de sinal l�gico alto do PWM2   
}
void main(){
    Definir_PINOS();
    InitializeUSART();
    Definir_PWM();
    configTimer0();
    configTimer1();
    configTimer2();
    configTimer3();
    while(1){
        if(flag_controle == 1){//fazer o controle do motor aqui
            freq_M1 = contador_M1*0.0004;
            freq_M2 = contador_M2*0.0004;
            RPM_M1 = 60000/(freq_M1*reduction_M1);
            RPM_M2 = 60000/(freq_M2*reduction_M2);
            if(Speed_M1 == 0){
                RPM_M1 = 0;
            }
            if(Speed_M2 == 0){
                RPM_M2 = 0;
            }
            flag_controle = 0;  
        }
        if(Flag_RX == 1)
        {
            Flag_RX = 0;
            Interpreta_RX();
        }
        //itoa(buffer,contador_M1,10); //Converte um inteiro para char
        sprintf(buffer_M1,"%.2f",RPM_M1); //Converte float para inteiro
        sprintf(buffer_M2,"%.2f",RPM_M2);
        putsUSART("motor 1: ");
        putsUSART(buffer_M1);
        putsUSART(" RPM ");
        putsUSART(" motor 2: ");
        putsUSART(buffer_M2);
        putsUSART(" RPM");
        putsUSART("\r\n"); 
        Controle_Motores(Rotacao_M1, Rotacao_M2); 
        setar_PWM();
    }
}

