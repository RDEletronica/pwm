/*
 * File:   main.c
 * Author: eduardo kamada
 *
 * Created on 28 de Mar�o de 2017, 23:53
 */
#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <plib/usart.h>
#include <plib/pwm.h>
#include <string.h>
#include "config.h"

//defines
#define _XTAL_FREQ 20000000
#define BAUDRATE 57600
#define BRG_VAL (_XTAL_FREQ)/(16*BAUDRATE)-1
#define TIMER0_START_REG 0x0000

//fun��es
void definir_PINOS();
void configTimer0();
void Definir_PWM();
void Definir_USART();
void interrupt Controle_RX();
void Interpreta_RX();
void Controle_Motores(int L1, INT L2);
void setar_pwm(int duty1, int duty2);

//var�aveis globais
int Speed_M1;
int Speed_M2;
int Rotacao_M1;
int Rotacao_M2;
int cntChar = 0;
int Flag_RX = 0;
int Size_RX = 0; 
int Flag_EXE = 0;
char RX_received[30];
//Configura��es de pinos

void interrupt Controle_RX()
{
    if(PIR1bits.RCIF == 1)
    {
        PIR1bits.RCIF = 0;
        if((cntChar == 0) & (ReadUSART() == '$')){
            Flag_RX = 1;
        }
        if(Flag_RX == 1){
            RX_received[cntChar] == ReadUSART();
            if(RX_received[cntChar] == '#'){
                Flag_RX = 0;
                RX_received[cntChar + 1] = '\0';
                Size_RX = cntChar - 1;
                cntChar = 0;
                Flag_EXE = 1 ;
            }
            else{
                cntChar++;
            }
        }
    }
}

void Definir_PINOS(void){
    //Definir os pinos A como sa�da
    TRISAbits.RA2 = 0; //Digital
    TRISAbits.RA3 = 0; //Digital
    TRISAbits.RA4 = 0; //Digital
    TRISAbits.RA5 = 0; //Digital
    TRISBbits.RB1 = 0; //Digital
    TRISBbits.RB2 = 0; //Digital
    //Definir os pinos de pwm como sa�da
    TRISCbits.RC1 = 0; //Digital
    TRISCbits.RC2 = 0; //Digital
    TRISCbits.RC6 = 0; //Digital - PINO TX(Transmissor)
    TRISCbits.RC7 = 1; //Digital - PINO RX(Receptor)
    //Definir pino do led como sa�da
    
    TRISBbits.RB0 = 0; //Digital
    //Led em n�vel l�gico baixo ao iniciar
    
    ADCON1bits.PCFG0 = 1; //Digital
    ADCON1bits.PCFG1 = 0; //Digital
    ADCON1bits.PCFG2 = 1; //Digital
    ADCON1bits.PCFG3 = 1; //Digital
    
    RCONbits.IPEN = 1;
    INTCONbits.PEIE = 0;
    INTCONbits.GIE = 1;
    INTCONbits.TMR0IE = 1;
    
}

void configTimer0()
{
    
    T0CONbits.TMR0ON = 1;                   //Ativar o Timer1
    OpenTimer0(TIMER_INT_OFF & T0_8BIT & T0_SOURCE_INT & T0_PS_1_16);
    WriteTimer0(TIMER0_START_REG);
    
}

void Definir_PWM(){
    T2CONbits.TMR2ON=1;
    OpenTimer2(TIMER_INT_OFF & T0_PS_1_16); //Setar prescale
    OpenPWM1(0xFF); //setar per�odo do pwm1
    OpenPWM2(0xFF); //setar per�odo do pwm2
    
}
void Definir_USART(){ 
    OpenUSART(USART_TX_INT_OFF & 
            USART_RX_INT_ON & 
            USART_ASYNCH_MODE & 
            USART_EIGHT_BIT & 
            USART_CONT_RX & 
            USART_BRGH_HIGH, BRG_VAL);
}


void Interpreta_RX(){
        if (Size_RX == 13)      //recep��o de mensagem de tamanho 13 caracteres
        {
            Rotacao_M1 = RX_received[1] - 48;
            Rotacao_M2 = RX_received[8] - 48;
            Speed_M1 = (RX_received[3] - 48)*1000 + (RX_received[4] - 48)*100 + (RX_received[5] - 48)*10 + (RX_received[6] - 48);
            Speed_M2 = (RX_received[10] - 48)*1000 + (RX_received[11] - 48)*100 + (RX_received[12] - 48)*10 + (RX_received[13] - 48);
            Flag_EXE = 1;
            PORTBbits.RB0 = !PORTBbits.RB0;
            Flag_EXE = 0;
        }
        
}

void Controle_Motores(int L1, INT L2){
    if(L1 == 1){
        PORTAbits.RA2 = 1;     
        PORTAbits.RA3 = 0;
    }else if(L1 == 0){
        PORTAbits.RA2 = 0; 
        PORTAbits.RA3 = 1;
    }
      if(L2 == 1){
        PORTAbits.RA4 = 1;     
        PORTAbits.RA5 = 0;
      }else if(L2 == 0){
        PORTAbits.RA4 = 0; 
        PORTAbits.RA5 = 1;
      }   
}


void setar_pwm(int duty1, int duty2)
{
    SetDCPWM1(duty1);       //Comando para setar o "tempo" de sinal l�gico alto do PWM1
    SetDCPWM2(duty2);       //Comando para setar o "tempo" de sinal l�gico alto do PWM2
}
void main(){
    Definir_PINOS();
    configTimer0();
    Definir_PWM();
    Definir_USART();
    while(1){
        Controle_Motores(Rotacao_M1, Rotacao_M2);
        setar_pwm(Speed_M1, Speed_M2);   
        if(Flag_EXE == 1)
        {
            Interpreta_RX();
            Flag_EXE = 0;
        }
    }
    return;
}
    